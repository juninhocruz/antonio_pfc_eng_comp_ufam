# Sobre o TCC


* Para download do projeto em [PDF](https://bitbucket.org/juninhocruz/antonio_pfc_eng_comp_ufam/raw/855658c11b1734c679163f668fece6f52f761044/main_tcc.pdf),  [clique aqui](https://bitbucket.org/juninhocruz/antonio_pfc_eng_comp_ufam/raw/855658c11b1734c679163f668fece6f52f761044/main_tcc.pdf).


# Sobre o Autor


...



# Sobre o Template 



* TCC Eng. Computação UFAM V2



Template em Latex para alunos de Engenharia da Computação da UFAM. Pode também ser utilizado por outros alunos, de outros cursos ou outras faculdades. Fiquem à vontade. :D



## Dicas



* Existem vários editores de Latex. Após utilizar alguns ([TexMaker](http://www.xm1math.net/texmaker/) e [TexStudio](http://www.texstudio.org/)), vimos que o mais estável é o [TexStudio](http://www.texstudio.org/). Quando forem desenvolver seus trabalhos, procurem optar por este editor.



* Dê preferência também para o Sistema Operacional Linux, como o [Mint](https://www.linuxmint.com/). Isso facilita a instalação dos editores e também das bibliotecas.



* Para instalar TODAS as libs necessárias para rodar o LaTex, rode o comando no linux `sudo apt-get install texlive-full` (Em distribuições baseadas no [Debian](https://www.debian.org/index.pt.html)).



## Autores


### Este Trabalho foi Desenvolvido por António César Vieira da Cruz Júnior


### Template por Arllem Farias


### Revisado por Victor Lopes

