\chapter{Fundamentação Teórica} \label{cap2}


Este capítulo aborda, de forma sucinta, os fundamentos teóricos que foram aplicados no planejamento e implementaçao deste projeto.



\section{O Som}

Os sons são ondas mecânicas causadas pela vibração de objetos. Essa vibração resulta na variação de pressão no ar, que pode ser captada pelo ouvido e interpretada pelo cérebro. Os seres humanos são capazes de ouvir vibrações entre 20 Hz e 20 kHz \cite{benson2006}. Um som pode ser caracterizado pela sua altura, intensidade, duração e timbre. Em relação a altura, um som pode ser grave (vibrações de baixa frequência) ou agudo (vibrações de alta frequência). Quanto a intensidade, os sons podem ser fortes ou fracos, sendo os sons fortes aqueles que transportam grande quantidade de energia. Por fim, o timbre é a característica mais particular de um som. Por meio dele, é possível discernir entre uma nota dó tocada por um violino de uma nota dó tocada por um piano. A Figura \ref{fig-teoria-som} ilustra a diferença entre as vibrações desses dois instrumentos.


\begin{figure}[h]
	\centering
	\includegraphics[width=\linewidth/2]{pasta1_figuras/rect4664.png}
	\caption{Comparativo entre timbre de violino e piano.}
	\label{fig-teoria-som}
\end{figure}


\section{Frequência Fundamental}

A frequência fundamental $f_0$ é a menor frequência de uma vibração, ou também, a primeira frequência de uma série harmônica. Uma série harmônica é o conjunto de ondas composto pela $f_0$ e suas múltiplas inteiras. Normalmente, todo estímulo sonoro é formado por uma série harmônica. A frequência fundamental está diretamente ligada à altura de um som e, por meio dela, distingue-se grave e agudo.


\section{Pitch}


O $pitch$ é um conceito musical relacionado à frequência fundamental, entretanto voltado para a percepção humana do som. Ele é definido em \cite{hartmann1996} como \textit{``o atributo pelo qual a audição pode ordenar os sons em uma escala de grave para agudo, dependendo principalmente do conteúdo de frequência do estímulo sonoro, mas também da forma de onda desse estímulo''}. Embora pareça confusa, esta definição abrange a relação do $pitch$ com a altura e com o timbre de um som, necessário para que o conceito seja válido em todos os casos, tanto em aplicações musicais como em aplicações de reconhecimento de fala. Em \cite{klapuri2006}, fica evidente que o $pitch$ de um som refere-se a frequência em que uma onda senoidal é reconhecida por um ouvido humano como correspondente ao som em questão.


\section{Detecção Automática de $f_0$}

Embora muitos trabalhos tratem frequência fundamental e $pitch$ como sinônimos, \cite{gerhard2003} explica de modo claro que existem diferenças consideráveis entre os dois conceitos, de modo que um sistema de detecção de $pitch$ objetiva resultados diferentes de um sistema de detecção de $f_0$. Como exemplo disso, pode-se observar que um sinal com frequências mais puras e poucas informações de timbre são ideais para a detecção de $f_0$, entretanto, a detecção de $pitch$ depende ainda da intensidade, duração e timbre.


Muitas soluções e métodos de detecção automática de $f_0$ já foram desenvolvidas e apresentadas, obtendo resultados positivos. Entretanto, \cite{gerhard2003} também afirma que é difícil que uma mesma solução de detecção de $f_0$ tenha bom desempenho tanto aplicações de música quanto em aplicações de fala. Deste modo, primeiro torna-se necessário definir o tipo de aplicação do sistema de detecção para, só então, desenvolver e avaliar o desempenho desse sistema no contexto escolhido.


\section{Digitalização de um Sinal de Som}

Para a digitalização de um sinal sonoro, são necessários a amostragem, a quantização e a codificação. 

\subsection{Amostragem}
A amostragem é o processo de conversão de um sinal contínuo no tempo em um sinal discreto. Se a discretização no tempo ocorrer em uma frequência de amostragem ($f_s$) muito baixa, as altas frequências não serão amostradas, gerando o efeito \textit{aliasing}, que é a distorção de um sinal causado pela perda das informações de alta frequência. 


Segundo o Teorema da Amostragem\cite{lathi2007}, dado um sinal contínuo amostrado com frequência $f_s$, se $f_s > 2f_M$, onde $f_M$ é a frequência máxima do sinal, então será possível recuperar toda a informação do sinal a partir dos valores amostrados. O intervalo entre a coleta de cada amostra é denominado \textit{período de amostragem}, representado pelo símbolo $T_s$. Pela relação de período e frequência, temos que $T_s = \frac{1}{f_s}$.


A Figura \ref{fig-sampling} demonstra graficamente a amostragem de um sinal.


\subsection{Quantização}
A quantização, por sua vez, consiste em selecionar um conjunto de valores finitos, espaçados linearmente ou não, para os quais as amostras terão seus valores de amplitude arredondados. A diferença entre o valor real e o valor quantizado da amostra é chamado de erro de quantização. Na digitalização de sinais, deve-se adotar um conjunto de quantização com tamanho suficientemente grande para que as perdas de informação por erros nesse procedimento não impeçam uma posterior recuperação do sinal.


A Figura \ref{fig-quantizacao} demonstra graficamente a quantização de um sinal.

\begin{figure}[h]
	\begin{subfigure}{0.5\textwidth}
		\centering
		\includegraphics[scale=0.4]{pasta1_figuras/amostragem.png}
		\caption{Amostragem} \label{fig-sampling}
	\end{subfigure}
	\hspace*{\fill} % separation between the subfigures
	\begin{subfigure}{0.5\textwidth}
		\centering
		\includegraphics[scale=0.75]{pasta1_figuras/quantization.png}
		\caption{Quantização} \label{fig-quantizacao}
	\end{subfigure}
	\caption{Amostragem e Quantização}
\end{figure}

\subsection{Codificação}
Por fim, a codificação é o procedimento no qual se define o formato de armazenamento das informações do sinal. Após esse passo, essa representação digital passa a ser denominada áudio. Existem diferentes codificadores para áudio, cada um deles com suas particularidades. Alguns codificadores usam técnicas de compressão, que podem gerar pequenas perdas de informação, enquanto outros buscam preservar a integridade da representação, sendo, consequentemente, maiores em tamanho de arquivo. O Matlab\rreg trabalha nativamente com 8 tipos de codificadores de áudio: \textit{WAVE, OGG, FLAC, AU, AIFF, AIFC, MP3 e MPEG-4 AAC}\cite{mathworks2018audioread}.


\section{Notas Musicais}

Sob a ótica da música, os \textit{pitches} são representados pelas notas musicais. A distância entre a frequência de uma nota e metade ou dobro dessa mesma frequência é chamada de oitava. O menor intervalo entre duas notas é chamado de ``semitom'', enquanto um ``tom'' são dois semitons. A Tabela \ref{tab-notas} exibe todas as notas da 4ª oitava. A nota Lá (A) nessa oitava equivale a frequência de 440Hz, isso significa que essa mesma nota na 3ª oitava tem metade dessa frequência, ou seja, 220Hz. Por padrão, a nota lá da 4ª oitava, ou A4, é usada como base para a definição de todas as outras notas. A maioria dos músicos utiliza A4 em 440Hz, mas há casos em que são adotadas frequências diferentes como 436Hz e 444Hz.

\begin{table}[]
	\centering
	\begin{tabular}{|c|c|c|}
		\hline
		\textbf{Nota}                                           & \textbf{Abreviação} & \textbf{Frequência (em Hz)} \\ \hline
		Dó                                                      & C                   & 261.6                       \\ \hline
		\begin{tabular}[c]{@{}c@{}}Dó Sustenido ou Ré Bemol\end{tabular}  & C\# ou Db                 & 277.2                       \\ \hline
		Ré                                                      & D                   & 293.7                       \\ \hline
		\begin{tabular}[c]{@{}c@{}}Ré Sustenido ou Mi Bemol\end{tabular}  & D\# ou Eb                 & 311.1                       \\ \hline
		Mi                                                      & E                   & 329.6                       \\ \hline
		Fá                                                      & F                   & 349.2                       \\ \hline
		\begin{tabular}[c]{@{}c@{}}Fá Sustenido ou Sol Bemol\end{tabular}  & F\# ou Gb                 & 370.0                       \\ \hline
		Sol                                                     & G                   & 392.0                       \\ \hline
		\begin{tabular}[c]{@{}c@{}}Sol Sustenido ou Lá Bemol\end{tabular} & G\# ou Ab                 & 415.3                       \\ \hline
		Lá                                                      & A                   & 440.0                       \\ \hline
		Lá Sustenido ou Si Bemol                                            & A\# ou Bb                 & 466.2                       \\ \hline
		Si                                                      & B                   & 439.9                       \\ \hline
	\end{tabular}
	\caption{Notas musicais da 4ª oitava e suas frequências}
	\label{tab-notas}
\end{table}


\section{Ressonância}


A ressonância é um fenômeno físico em que uma vibração dá origem a outras vibrações com maior amplitude em um conjunto de frequências específicas. Essas frequências geradas são chamadas de frequências ressonantes. Os instrumentos musicais também podem apresentar esse fenômeno, como, por exemplo, alguns instrumentos de corda (violão, piano acústico, violino, entre outros) que utilizam uma caixa de ressonância para amplificar o som gerado pela vibração de suas cordas.


\section{Domínios}

Em PDS, os sinais são representados em dois diferentes domínios: tempo e frequência. No domínio do tempo, ou domínio temporal, um sinal é representado como amostragens de sua amplitude em intervalos de tempo $T$. Já no domínio da frequência, ou domínio espectral, um sinal é representado pela amplitude das oscilações que ocorrem em cada frequência. Desse modo, ter o mesmo sinal em diferentes domínios é tê-lo em diferentes perspectivas.


Os domínios do tempo e da frequência são permutáveis entre si, ou seja, dado um sinal $x(t)$ temporal, é possível transportar esse sinal para a representação espectral na forma $X(j\omega)$ sem que haja perda da informação. Do mesmo modo, é possível fazer o caminho inverso e retornar um sinal na frequência para a sua forma temporal inicial. Para transitar entre os domínios, utiliza-se as transformadas de Fourier.

\section{Transformada de Fourier}

A transformada de Fourier é uma ferramenta de decomposição de uma função em partes de base senoidal, ou seja, o sinal temporal passa a ser expresso em frequências. Inicialmente proposta pelo francês Jean Baptiste Joseph Fourier\cite{open1999}, as séries de Fourier deram origem a diferentes versões da transformada de Fourier, cada uma voltada para um tipo de sinal de origem.



Para sinais discretos no tempo utiliza-se a Transformada de Fourier em Tempo Discreto (DTFT, do inglês \textit{Discrete-Time Fourier Transform}), dada pela equação (\ref{eq_DTFT}):

\begin{equation} \label{eq_DTFT}
	X(\omega) = \sum_{n=-\infty}^{+\infty}x(n)e^{-j\omega n}
\end{equation}
onde $\omega$ é a frequência angular, em radianos por segundos, e $n$ é a posição da enésima amostra do sinal.


A DTFT é aplicável a sinais discretos no tempo, entretanto, é computacionalmente inviável, visto que sua saída é contínua na frequência. Deste modo, torna-se necessário discretizar essa saída para valores específicos de $\omega$. Para este fim, desenvolveu-se a Transformada Discreta de Fourier (DFT, do ingês \textit{Discrete Fourier Transform}).


\section{Transformada Discreta de Fourier}


A DFT é dada pela equação (\ref{eq_DFT}):

\begin{equation} \label{eq_DFT}
	X(k) = \sum_{n=0}^{N-1}x(n)e^{-j2\pi \frac{kn}{N}}
\end{equation}
onde x(n) é um sinal finito de tamanho N, e k assume valores inteiros de 0 a N-1.


Nesse contexto, X(k), DFT do sinal de entrada, é uma saída periódica. Essa discretização da saída aproxima a transformação de Fourier para a viabilidade computacional. Entretanto, executar uma soma do tamanho do sinal sob análise para cada valor específico da DFT,  ainda demanda um grande trabalho computacional, pois à medida em que aumenta-se o sinal de entrada, também aumenta-se quadraticamente a quantidade de operações. Por notação assintótica, pode-se afirmar que a DFT exige um esforço computacional $\Omega(N^2)$.


Por esse motivo, tornou-se necessário obter um algoritmo computacionamente mais eficiente para calcular a DFT: A Transformada Rápida de Fourier (FFT, do inglês \textit{Fast Fourier Transform}).


\section{Transformada Rápida de Fourier}

Designa-se como FFT uma série de algoritmos propostos que visam realizar a DFT de maneira mais eficiente. A Transformada Rápida de Fourier reduz as operações computacionais de $\Omega(N^2)$ para $\Omega( N log_2 N)$, tornando viável tal implementação. Para o presente projeto, utilizou-se a função nativa \textit{fft} do MATLAB\rreg, cujo a documentação fornece também casos básicos de uso\cite{mathworks2018}. Essa implementação baseia-se na equação:

\begin{equation} \label{eq-fft-matlab}
	X(k)=\sum_{j=1}^{N}x(j) W(N,j,k)
\end{equation}
onde $x(n)$ é o sinal de entrada, $N$ o tamanho do sinal, e $W$ é dado como:
\begin{equation} \label{eq-fft-matlab-w}
	W(N,j,k) = cos(\frac{2\pi i(j,k)}{N}) + j sen(\frac{2\pi i(j,k)}{N})
\end{equation}
onde,
\begin{equation} \label{eq-fft-matlab-i}
	i(j,k) = (j-1)(k-1)
\end{equation}

O tempo de execução dessa função depende do tamanho da entrada. Sinais com comprimentos que sejam potências de 2 são mais rápidos para o processamento, seguidos pelos sinais cujo o comprimento contém apenas pequenos fatores primos. As entradas que demoram mais para serem processadas são aquelas em que seu comprimento contém grandes fatores primos.


O tamanho e a precisão na frequência da saída de um algoritmo FFT também está diretamente relacionado ao tamanho do sinal de entrada. Em casos onde o sinal a ser transformado não possui um tamanho ideal para a precisão no domínio da frequência e tempo de execução desejados, pode-se adotar a técnica de \textit{padding}, acrescentando zeros ao final do sinal. 


\section{Transformada de Fourier de Tempo Curto}

A Transformada de Fourier de Tempo Curto (STFT, do inglês \textit{Short-time Fourier Transform}), é uma técnica de aplicação da transformada de Fourier utilizada para a análise de sinais não periódicos. Ela consiste na utilização de uma janela de tempo curto, que é colocada sobre o início do sinal. O segmento interno da janela é considerado como periódico e é submetido à transformada de Fourier. Após, desloca-se a janela de tempo curto e repete-se o processo até que a janela alcance o fim do sinal. Desse modo, é possível entender como o espectro de um sinal se comporta a cada instante de tempo. 


A STFT discreta pode ser implementada utilizando a FFT. O comprimento das janelas de tempo curto precisa ser definido conforme o tipo de aplicação desejada, pois deve ser compatível com o tamanho do fenômeno que deseja-se observar. Outro fator importante a ser definido é a forma de deslocamento da janela. Quando o tamanho da janela é igual ao valor de deslocamento, todas as amostras são analisadas uma única vez. Se o tamanho da janela for menor que o deslocamento, algumas amostras não serão analisadas, e se o deslocamento for maior, cada amostra será analisada em mais de uma posição da janela.


Em PDS, mais precisamente em sistemas de reconhecimento e detecção por áudio, essa técnica se torna imprescindível, pois permite uma visão clara da informação contida no sinal. 

\section{Espectrograma}

O espectrograma é uma forma de representar sinais. Ele mostra como o sinal evolui na frequência conforme o tempo. Existem diferentes formatos de gráfico para um espectrograma. A Figura \ref{fig-spectrogram} demonstra dois desses formatos: (a) e (b) Gráfico bidimensional com escala de cores, onde o eixo do tempo está na horizontal, o eixo da frequência na vertical e a amplitude é representada pela escala de cores; (c) e (d) Gráfico tridimensional clássico $z = f(x,y)$, onde o tempo está no eixo x, y é a frequência e z é a amplitude;

\begin{figure}[h]
	\begin{subfigure}{0.5\textwidth}
		\includegraphics[scale=0.35]{pasta1_figuras/spectrogram1.png}
		\caption{}
	\end{subfigure}
	\hspace*{\fill} % separation between the subfigures
	\begin{subfigure}{0.5\textwidth}
		\includegraphics[scale=0.462]{pasta1_figuras/spectrogram4.png}
		\caption{}
	\end{subfigure}

	\begin{subfigure}{0.5\textwidth}
		\includegraphics[scale=0.225]{pasta1_figuras/spectrogram2.png}
		\caption{}
	\end{subfigure}
	\hspace*{\fill} % separation between the subfigures
	\begin{subfigure}{0.5\textwidth}
	\includegraphics[scale=0.44]{pasta1_figuras/spectrogram3.png}
	\caption{}
	\end{subfigure}
	 \caption{Espectrogramas no MATLAB\rreg} \label{fig-spectrogram}
\end{figure}

O método mais usual para a geração do espectrograma de um sinal é por meio da STFT. Aplicando a técnica, calcula-se a FFT de cada janela do sinal de entrada. Como a saída da FFT pode conter números complexos, computa-se o módulo dos valores de saída da transformada, obtendo-se vários planos de frequências e amplitudes. Por fim, os planos de espectro de cada janela de tempo são colocados lado a lado, formando assim o espectrograma completo.


A equação (\ref{eq-spectrogram}) descreve esse processo com o uso da STFT:

\begin{equation}
	\label{eq-spectrogram}
	spectrogram(x(n), \Delta) = |STFT(x(n),\Delta)|^2
\end{equation}
onde $x(n)$ é o sinal sob análise e $\Delta$ é o tamanho da janela para a STFT.


O MATLAB\rreg fornece o método \textit{spectrogram} para obter, por meio da STFT, o espectrograma de um sinal\cite{mathworks2018spectrogram}. Este método utiliza por padrão o gráfico bidimensional com escala de cores.

%[Detecção de oitavas]
%[Problemas de continuidade]

%\section{Ruído}

%Indicar quais são os quais são os principais fundamentos (algoritmos, paradigmas, teorias) a serem empregados.
%Cada fundamento utilizado deve ser justificado.


% Fim Capítulo